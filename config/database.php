<?php
if (defined("IN_APPS") === false) exit("Access Dead");

$config['database'] = array(
	'dsn'      => 'sqlite:diagnosis.db',
	'username' => '',
	'password' => '',
	'prefix'   => '',
);