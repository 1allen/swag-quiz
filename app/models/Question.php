<?php

class Question extends Model {
	public static $_table = 'questions';

	public function answers() {
		return $this->has_many('Answer', 'question_id');
	}

}