<?php

class Answer extends Model
{
	public static $_table = 'answers';

	public function question()
	{
		return $this->belongs_to('Question');
	}

	/**
	 * Проверка правильности ответа
	 * @param $questionId айди вопроса
	 * @param $answerNum номер ответа
	 * @return bool
	 */
	public static function isCorrect($questionId, $answerNum)
	{
		return Model::factory('Answer')
			->select('is_correct')
			->where_equal('question_id', $questionId)
			->where_equal('num', $answerNum)
			->find_one()
			->is_correct == 1;
	}

	/**
	 * Все правильные ответы
	 * @return array
	 */
	public static function correctAnswers()
	{
		$q = Model::factory('Answer')
			->select('question_id')
			->select('answer_num', 'correct')
			->where('is_correct', 1)
			->find_array();

		return array_column($q, 'correct', 'answer_num');
	}
}