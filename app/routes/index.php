<?php

$app->get('/', function () use ($app) {

	$questions = Model::factory('Question')->find_many();
	foreach ($questions as $q) {
		$q->answers = $q->answers()->find_array();
	}

	$app->render('quiz.twig', array(
		'config' => $app->config('config'),
		'questions' => $questions,
	));
});

$app->get('/stats(/)', function () use ($app) {

	$res = array();
	$q = Model::factory('Diagnosis')
		->select_expr('count(*)', 'cnt')
		->select_expr('percentage')
		->group_by('percentage')
		->find_array();

	$q = array_column($q, 'cnt', 'percentage');

	for ($i = 0; $i <= 100; $i++) {
		$res[] = array($i, isset($q[$i]) ? (int)$q[$i] : 0);
	}

	$app->render('stats.twig', array('stats' => $res));
});

$app->post('/ajax(/)', function () use ($app) {
	$score = 0;

	$questions = Model::factory('Question')->select('id')->find_array();
	$questions = array_column($questions, 'id');
	foreach ($questions as $id) {

		if ($answer = W::getVal('q' . $id, $_POST)) {
			if (Answer::isCorrect($id, $answer))
				$score++;
		} else
		{
			$app->render('diagnosis.twig', array('results' => array('diagnosis' => 'Почему-то не все вопросы отвечены. Такое, увы, случается.')));
			return;
		}
	}

	$percentage = round( $score / sizeof($questions) * 100 );


	$diagnosis = Model::factory('Diagnosis')->create();
	$diagnosis->percentage = $percentage;
	$diagnosis->time = time();
	$diagnosis->ip = ip2long($_SERVER['REMOTE_ADDR']);
	$diagnosis->save();

	$app->render('diagnosis.twig',
		array(
			'results' => array(
				'percentage' => $percentage,
				'diagnosis' => Diagnosis::conclusion($percentage),
			)
		)
	);
});

// should be unused
$app->get('/import', function () {

	// cleanup
	// почему-то работает через раз
	ORM::raw_execute('delete from answers;delete from questions;update sqlite_sequence set seq = null where `name` = "answers" or `name` = "questions"');

	$data = file_get_contents('public/javascripts/data.js');
	$data = substr($data, 15, -1); // strip var assignment
	$data = preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t](//).*)#", '', $data); // strip comments
	$data = json_decode($data);

	foreach ($data->questions as $qnum => $q) {
		$question = Model::factory('Question')->create();
		$question->text = $q->question;
		$question->save();

		foreach ($q->answers as $anum => $atext) {
			$answer = Model::factory('Answer')->create();
			$answer->num = ++$anum; // from 1
			$answer->question_id = $question->id;
			$answer->text = $atext;
			$answer->is_correct = Diagnosis::$correctAnswers[$qnum] == $anum;
			$answer->save();
		}
	}
	echo 'done!';
});

$app->get('/vasya', function () {

});