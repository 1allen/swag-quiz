<?php

/**
 * Class W
 */
class W
{
	/**
	 * @param $var
	 * @param bool $die
	 */
	public static function dump($var, $die = true)
	{
		var_dump($var);
		if ($die)
			die();
	}

	/**
	 * @param $val
	 * @param null $default
	 * @return null
	 */
	public static function get($val, $default = null)
	{
		return isset($val) ? $val : $default;
	}

	/**
	 * @param $key
	 * @param $from
	 * @param null $default
	 * @return mixed
	 */
	public static function getVal($key, $from, $default = null)
	{
		return self::_getValueByComplexKeyFromArray($key, $from, $default);
	}

	/**
	 * Возвращает значения ключа в заданном массиве
	 * @param string $key Ключ или ключи точку
	 * Например, 'Media.Foto.thumbsize' преобразуется в ['Media']['Foto']['thumbsize']
	 * @param array $array Массив значений
	 * @param mixed $defaultValue Значение, возвращаемое в случае отсутствия ключа
	 * @return mixed
	 */
	protected static function _getValueByComplexKeyFromArray($key, $array, $defaultValue = null)
	{
		if (strpos($key, '.') === false) {
			return (isset($array[$key])) ? $array[$key] : $defaultValue;
		}

		$keys = explode('.', $key);

		if (!isset($array[$keys[0]])) {
			return $defaultValue;
		}

		$value = $array[$keys[0]];
		unset($keys[0]);

		foreach ($keys as $k) {
			if (!isset($value[$k]) && !array_key_exists($k, $value)) {
				return $defaultValue;
			}
			$value = $value[$k];
		}

		return $value;
	}
}