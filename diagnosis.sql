PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "diagnosis" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "percentage" integer NOT NULL,
  "time" integer NOT NULL,
  "ip" integer NOT NULL
);
ANALYZE sqlite_master;
CREATE TABLE "questions" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "text" text(128) NOT NULL
);
INSERT INTO "questions" VALUES(1,'Как называется "южный" стиль рэпа?');
INSERT INTO "questions" VALUES(2,'Выбери лишнее');
INSERT INTO "questions" VALUES(3,'В чьем творчестве чаще используется слово SWAG?');
INSERT INTO "questions" VALUES(4,'Какого цвета легендарный хьюстонский дранк?');
INSERT INTO "questions" VALUES(5,'Вставь пропущенное слово: "REAL TRAP …"');
INSERT INTO "questions" VALUES(6,'Как называется последний альбом тРэп-исполнителя Waka Flocka Flame?');
INSERT INTO "questions" VALUES(7,'Что такое "1017 Brick Squad"?');
INSERT INTO "questions" VALUES(8,'Популярный TRAP диджей и продюссер, псевдоним которого - название автоматического оружия?');
INSERT INTO "questions" VALUES(9,'Какая тачка чаще всего снится настоящему хаслеру?');
INSERT INTO "questions" VALUES(10,'Как расшифровывается абривиатура "A$AP"?');
INSERT INTO "questions" VALUES(11,'При каких обстоятельствах умер Тупак Амару Шакур?');
INSERT INTO "questions" VALUES(12,'Что такое «блинг»?');
INSERT INTO "questions" VALUES(13,'Имя будущего ребёнка Kanye West''a?');
INSERT INTO "questions" VALUES(14,'Gucci Mane знаменит татуировкой на лице в виде:');
INSERT INTO "questions" VALUES(15,'"Пена" на нигерском жаргоне означает');
INSERT INTO "questions" VALUES(16,'Компания "Pelle Pelle" известна своими');
INSERT INTO "questions" VALUES(17,'Что такое/кто такой «дюрэг»?');
INSERT INTO "questions" VALUES(18,'Под каким именем раньше знали рэппера 2Chainz?');
INSERT INTO "questions" VALUES(19,'Образ какого святого эксплуатирует бренд "True Religion"?');
INSERT INTO "questions" VALUES(20,'Что нужно держать по дальше от Based God''а?');
INSERT INTO "questions" VALUES(21,'Легенда гласит, что именно это означают свисающие с проводов кроссы:');
CREATE TABLE "answers" (
  "id" integer NULL PRIMARY KEY AUTOINCREMENT,
  "question_id" integer NOT NULL,
  "text" text NOT NULL,
  "is_correct" integer NOT NULL DEFAULT '0',
  "num" integer NOT NULL,
  FOREIGN KEY ("question_id") REFERENCES "questions" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);
INSERT INTO "answers" VALUES(1,1,'trup','',1);
INSERT INTO "answers" VALUES(2,1,'trap',1,2);
INSERT INTO "answers" VALUES(3,1,'trip','',3);
INSERT INTO "answers" VALUES(4,1,'trep','',4);
INSERT INTO "answers" VALUES(5,2,'Supreme','',1);
INSERT INTO "answers" VALUES(6,2,'Obey','',2);
INSERT INTO "answers" VALUES(7,2,'Black Scale','',3);
INSERT INTO "answers" VALUES(8,2,'Columbia',1,4);
INSERT INTO "answers" VALUES(9,3,'Lil B',1,1);
INSERT INTO "answers" VALUES(10,3,'Puff Daddy','',2);
INSERT INTO "answers" VALUES(11,3,'Джастин Бибер','',3);
INSERT INTO "answers" VALUES(12,3,'Тимати','',4);
INSERT INTO "answers" VALUES(13,4,'жёлтого','',1);
INSERT INTO "answers" VALUES(14,4,'зелёного','',2);
INSERT INTO "answers" VALUES(15,4,'фиолетового',1,3);
INSERT INTO "answers" VALUES(16,4,'бесцветный','',4);
INSERT INTO "answers" VALUES(17,5,'pitch','',1);
INSERT INTO "answers" VALUES(18,5,'bitch','',2);
INSERT INTO "answers" VALUES(19,5,'sh*t',1,3);
INSERT INTO "answers" VALUES(20,5,'cheat','',4);
INSERT INTO "answers" VALUES(21,6,'Triple F Life: Friends, Fans, Family',1,1);
INSERT INTO "answers" VALUES(22,6,'Finally Rich','',2);
INSERT INTO "answers" VALUES(23,6,'Peso','',3);
INSERT INTO "answers" VALUES(24,6,'The Boss','',4);
INSERT INTO "answers" VALUES(25,7,'музыкальный лэйбл',1,1);
INSERT INTO "answers" VALUES(26,7,'ночной клуб','',2);
INSERT INTO "answers" VALUES(27,7,'напиток','',3);
INSERT INTO "answers" VALUES(28,7,'шоколадка','',4);
INSERT INTO "answers" VALUES(29,8,'M16','',1);
INSERT INTO "answers" VALUES(30,8,'UZ',1,2);
INSERT INTO "answers" VALUES(31,8,'AK47','',3);
INSERT INTO "answers" VALUES(32,8,'STG44','',4);
INSERT INTO "answers" VALUES(33,9,'Mini Cooper','',1);
INSERT INTO "answers" VALUES(34,9,'Chevrolet Impala',1,2);
INSERT INTO "answers" VALUES(35,9,'Dodge Caravan','',3);
INSERT INTO "answers" VALUES(36,9,'ВАЗ 2114','',4);
INSERT INTO "answers" VALUES(37,10,'Always Strive and Prosper',1,1);
INSERT INTO "answers" VALUES(38,10,'All Sailors are Pirates','',2);
INSERT INTO "answers" VALUES(39,10,'Always Sunny at Philadelphia','',3);
INSERT INTO "answers" VALUES(40,10,'Always Swing and Push','',4);
INSERT INTO "answers" VALUES(41,11,'спрыгнул с 16-го этажа','',1);
INSERT INTO "answers" VALUES(42,11,'умер от передозировки наркотиками','',2);
INSERT INTO "answers" VALUES(43,11,'схватил вражескую пулю',1,3);
INSERT INTO "answers" VALUES(44,11,'сгнил в тюрьме','',4);
INSERT INTO "answers" VALUES(45,12,'Яркое украшение',1,1);
INSERT INTO "answers" VALUES(46,12,'Припев в хип-хопе','',2);
INSERT INTO "answers" VALUES(47,12,'Сорт марихуаны','',3);
INSERT INTO "answers" VALUES(48,12,'Дорогой смартфон','',4);
INSERT INTO "answers" VALUES(49,13,'East','',1);
INSERT INTO "answers" VALUES(50,13,'West','',2);
INSERT INTO "answers" VALUES(51,13,'North',1,3);
INSERT INTO "answers" VALUES(52,13,'South','',4);
INSERT INTO "answers" VALUES(53,14,'пистолета','',1);
INSERT INTO "answers" VALUES(54,14,'мороженки',1,2);
INSERT INTO "answers" VALUES(55,14,'Барта Симпсона','',3);
INSERT INTO "answers" VALUES(56,14,'креста','',4);
INSERT INTO "answers" VALUES(57,15,'смузи из антидепрессантов','',1);
INSERT INTO "answers" VALUES(58,15,'модель Nike Foamposite',1,2);
INSERT INTO "answers" VALUES(59,15,'синоним роскоши\богатства','',3);
INSERT INTO "answers" VALUES(60,15,'телка с большой задницей','',4);
INSERT INTO "answers" VALUES(61,16,'кастомными сникерами','',1);
INSERT INTO "answers" VALUES(62,16,'командными бейсболками','',2);
INSERT INTO "answers" VALUES(63,16,'автомобильными дисками','',3);
INSERT INTO "answers" VALUES(64,16,'кожаными куртками',1,4);
INSERT INTO "answers" VALUES(65,17,'головной убор',1,1);
INSERT INTO "answers" VALUES(66,17,'баскетбольный маневр','',2);
INSERT INTO "answers" VALUES(67,17,'резинка на трусах','',3);
INSERT INTO "answers" VALUES(68,17,'смотрящий по району','',4);
INSERT INTO "answers" VALUES(69,18,'Mama''s boy','',1);
INSERT INTO "answers" VALUES(70,18,'Tity Boy',1,2);
INSERT INTO "answers" VALUES(71,18,'Booty Boy','',3);
INSERT INTO "answers" VALUES(72,18,'Sissy Boy','',4);
INSERT INTO "answers" VALUES(73,19,'Исуса','',1);
INSERT INTO "answers" VALUES(74,19,'Будды',1,2);
INSERT INTO "answers" VALUES(75,19,'Мухаммеда','',3);
INSERT INTO "answers" VALUES(76,19,'Джа','',4);
INSERT INTO "answers" VALUES(77,20,'оружие','',1);
INSERT INTO "answers" VALUES(78,20,'лопатник','',2);
INSERT INTO "answers" VALUES(79,20,'пароли','',3);
INSERT INTO "answers" VALUES(80,20,'тёлок',1,4);
INSERT INTO "answers" VALUES(81,21,'Здесь кого-то убили',1,1);
INSERT INTO "answers" VALUES(82,21,'Здесь живет дилер',1,2);
INSERT INTO "answers" VALUES(83,21,'Здесь чтут понятия','',3);
INSERT INTO "answers" VALUES(84,21,'Здесь не рады чужакам','',4);
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('diagnosis',79);
INSERT INTO "sqlite_sequence" VALUES('questions',21);
INSERT INTO "sqlite_sequence" VALUES('answers',NULL);
CREATE UNIQUE INDEX "diag_idx" on diagnosis (id ASC);
CREATE UNIQUE INDEX "quest_idx" ON "questions" ("id");
CREATE UNIQUE INDEX "ans_idx" ON "answers" ("id");
COMMIT;
