(function () {		// namespace
	var currentq = $('div.question:first').data('questionId');
	var questionsLength = $('div.question').length;
	$(window)
		.load(function () {
			$('#questions-wrapper')
				.adaptQuizSize()
				.css('overflow', 'hidden');
			scroll();
			centerVertically();
			$('#quiz-form').ajaxForm(
				{
					url: 'ajax',
					success: function (data, statusText, xhr, $form) {
						$('#quiz-area')
							.html(data);
						initShare();
					},
					error: function () {
						alert('ошибка проверки результатов');
					}
				}
			);
			$('body')
				.on('click', '#start', function () {
					$('#intro').hide();
					$('#quiz-form').fadeIn();
					scroll();
				})
				.on('click', '#next', function () {
					if (currentq < questionsLength)
						currentq++;
					else {
						$('#quiz-form').submit();
					}
					scroll();
				})
				.on('click', '#rand', function () {
					$('.question').each(function () {
						$(this)
							.find(':radio')
							.eq(Math.floor(Math.random() * 4))
							.click();
					});
					currentq = questionsLength;
					scroll();
				})
				.on('change', '.answers :radio', function () {
					$('#next')
						.attr('disabled', $('#q' + currentq).find(':radio:checked').length == 0);
				})
			;

		})
		.resize(function () {
			scroll();
			centerVertically();
		})
	;

	function scroll() {
		$('#questions-wrapper')
			.adaptQuizSize()
			.scrollTo($('#q' + currentq), 500);
		$('#next').attr('disabled', $('#q' + currentq).find(':radio:checked').length < 1);
	}

	function centerVertically() {
		$elm = $('div.container > div.row');
		$elm.css({'margin-top': ($(window).height() - $elm.height()) / 2, 'top': '0'});
	}

	$.fn.extend({
		'adaptQuizSize': function () {
			$(this)
				// current div with overflow hidden
				.css('height', $('#q' + currentq).height() + 'px')
				// parent -- set max as possible
				.parent('div').css('height', Math.max.apply(null, $('div.question').map(function () { return $(this).height();}).get()) + 'px');
			return $(this);
		}
	});

	function initShare() {
		var shareid = 'yashare';
		var $share = $('#' + shareid);
		var YaShareInstance = new Ya.share({
			element: shareid,
			l10n: $share.data('yasharel10n'),
			elementStyle: {
				type: $share.data('yasharetype'),
				quickServices: $share.data('yasharequickservices').split(',')
			},
			title: $share.data('yasharetitle'),
			description: $share.data('yasharedescription')

		});
	}
})();